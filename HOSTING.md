# Firebase Hosting

Firebase will provide free hosting upto certain limit depends on the build files size and data-transfer happened through the firebase CDN. Then they will charge. [About Firebase pricing and Quotas.](https://firebase.google.com/pricing#hosting)

### Let's create build for your project by run the script

```
yarn build
or
npm build
```

### Create a project from Google Firebase Console.

### Install Firebase Globally

```sh
npm install -g firebase-tools
```

### Login to your firebase Account.

```sh
firebase login
```

### If it shows different account Please log-out and login proper account

```sh
firebase logout
```

### Initialise firebase in your project

```sh
firebase init
```

### Select hosting from the list (By press **_space_** and **_Enter_**)

### **_Select the Project_** which you created in Firebase console for this current project

### **Write your public folder name** as **_build_**

### Don't overwrite the existing **_index.html_** file, Which is in build folder

### Let's DEPLOY

```sh
firebase deploy
```

Congratulations..... You are successfully deployed your app!!!
