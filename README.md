# Kick start React project in type-script from scractch

#### Initialize **_git_** in your project

Run the following command to create **_.git_** folder in your project. Which going to maintain all your project history and commits

```sh
git init
```

#### Create a new File name as **_.gitignore_** - in project level

Where you will declare what are the folders and files to be excluded while you push code to **_git_**

#### Create a folder named as **_src_** - in project level

Where you going to make your hands dirty

#### Create a folder named as **_build_** - in project level

There you find **_output_** for your work.

#### Run following command to create **_package.json_** - in project level

which will going to contain all the **project informations**, **dependencies**, **dev-dependencies**, **scrips**, **etc.,** for our project

```sh
yarn init --y
```

#### Create a **_index.html_** file - in project level

File which going to render our **SPA** app in your Browser.

#### Let's Add our first package or **_dependency_** for our project

```sh
yarn add react react-dom
```

- **react** - is used to access react features
- **react-dom** - is used to render our .js || .jsx || .tsx file in browser

#### Let's add our first **_dev-dependencies_** for our project

```sh
yarn add -D typescript @types/react @types/react-dom
```

- **typescript** - is the language we going to use
- **@types/react** - to access typescript package of react
- **@types/react-dom** - to access typescript package of react-dom

#### Create a new file as **_tsconfig.json_** - in project level

Where you can configure your project
Let's add the piece of code in

```sh
{
  "compilerOptions": {
    "target": "ES5" /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017', 'ES2018', 'ES2019', 'ES2020', or 'ESNEXT'. */,
    "module": "ESNext" /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', 'es2020', or 'ESNext'. */,
    "moduleResolution": "node" /* Specify module resolution strategy: 'node' (Node.js) or 'classic' (TypeScript pre-1.6). */ /* Type declaration files to be included in compilation. */,
    "lib": [
      "DOM",
      "ESNext"
    ] /* Specify library files to be included in the compilation. */,
    "jsx": "react-jsx" /* Specify JSX code generation: 'preserve', 'react-native', 'react' or 'react-jsx'. */,
    "noEmit": true /* Do not emit outputs. */,
    "isolatedModules": true /* Transpile each file as a separate module (similar to 'ts.transpileModule'). */,
    "esModuleInterop": true /* Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'. */,
    "strict": true /* Enable all strict type-checking options. */,
    "skipLibCheck": true /* Skip type checking of declaration files. */,
    "forceConsistentCasingInFileNames": true /* Disallow inconsistently-cased references to the same file. */,
    "resolveJsonModule": true
    // "allowJs": true /* Allow javascript files to be compiled. Useful when migrating JS to TS */,
    // "checkJs": true /* Report errors in .js files. Works in tandem with allowJs. */,
  },
  "include": ["src/**/*"]
}
```

### Add Entry point to our Project

#### Create **_index.tsx_** file - in **src** folder

Add the following piece of coding. Which will be the entry point of source folder so this will be rendered in browser

```sh
import ReactDOM from 'react-dom'
import App from './App'

ReactDOM.render(<App />, document.getElementById('root'))
```

#### Create **_App.tsx_** file - in **src** folder

Add the piece html coding to be rendered

```sh
const App = () => {
  return
      <h1>
        Hello This is sample App
      </h1>
}

export default App
```

# Integrate Babel in our project - Which going to be our Interpreter

#### Add the following **_dev-dependencies_**

- Babel is used to Interprete and convert our code to Browser understanding html page
- Which will copmpile all our **.js, .jsx, .ts, .tsx, .css, .html, .json and all other files** used in our project

```sh
yarn add -D @babel/core @babel/preset-env @babel/preset-react @babel/preset-typescript
```

#### Create a **_.babelrc_** file - in project level

#### Add the piece of code in **_.babelrc_** file

```sh
{
  "presets": [
    "@babel/preset-env",
    [
      "@babel/preset-react",
      {
        "runtime": "automatic"
      }
    ],
    "@babel/preset-typescript"
  ]
}

```

# Integrate Webpack package to our project

- Which going to bundle all our assets and output file

#### Run the following command to add **webpack** package to **_dev-dependencies_**

```sh
yarn add -D webpack webpack-cli webpack-dev-server html-webpack-plugin
```

#### Add the following **_dev-dependency_**

To transpile our code using babel & webpack

```sh
yarn add -D babel-loader
```

#### Creat folder **_webpack_** - in project level

#### Create file **_webpack.config.js_** inside **webpack** folder

#### Add the piece of coding in **_webpack.config.js_** file

- Which will determine Entry point for our project
- Which will determie the interpreting rules to our project

```sh
const path = require('path')

module.exports = {
  entry: path.resolve(__dirname, '..', './src/index.tsx'),
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      }
    ],
  },
  output: {
    path: path.resolve(__dirname, '..', './build'),
    filename: 'bundle.js',
  },
  stats: 'errors-only',
}
```

#### Write **_script_** in **package.json** file

- To run in browser
- To create build
- Other scrips

Write the piece of coding as follows

```sh
  "scripts": {
    "start": "webpack serve --config webpack/webpack.config.js --open",
    "build": "webpack --config webpack/webpack.config.js --open",
    "test": "echo \"Error: no test specified\" && exit 1",
  },
```

#### Add the following **_dev-dependency_** to load **css** and **css-styles**

```sh
yarn add css-loader styles-loader
```

#### Add the piece of coding in **_webpack.config.js_**

```sh
{
  test: /\.css$/,
  use: ['style-loader', 'css-loader']
}
```

# Add image files to your project - How to do it from scratch

#### Add a .png file in your **_src_** folder

Then import it in your **App.tsx** You will face some error. Let's fix it

#### Let's Create a file with extention of **_XXXX.d.ts_**. Let's say **_filename.d.ts_** - in your **_src_** folder

#### Add the following line in **_filename.d.ts_** file

```sh
declare module "*.png"
```

#### Add the following code in **_webpack.config.js_** in order to support the following entention files

```sh
{
  test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
  type: 'asset/resource',
}
```

#### Add the following line in **_filename.d.ts_** file

```sh
declare module "*.svg"
```

#### Add the following code in **_webpack.config.js_** in order to support the following entention files

```sh
{
  test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
  type: 'asset/inline',
}
```

# Creating Different Environments for your project

#### Create Four files inside your _webpack folder_

- **webpack.common.js**
- **webpack.config.js**
- **webpack.dev.js**
- **webpack.prod.js**

#### Tranfer code from _webpack.config.js_ to _webpack.common.js_

#### Add the following code in _webpack.dev.js_

```sh
const webpack = require("webpack")

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({"process.env.name": JSON.stringify("Kick start React App")})
  ]
}
```

#### Add the following code in _webpack.dev.js_

```sh
const webpack = require("webpack")

module.exports = {
  mode: 'development',
  devtool: 'cheap-module-source-map',
  plugins: [
    new webpack.DefinePlugin({"process.env.name": JSON.stringify("Kick start Typescript")})
  ]
}
```

#### Add the following code in _webpack.config.js_

```sh
const {merge} = require("webpack-merge")
const commonConfig = require("./webpack.common.js")

module.exports = (envVars) => {
  const {env} = envVars
  const envConfig = require(`./webpack.${env}.js`)
  const config = merge(commonConfig, envConfig)
  return config
}
```

#### Add Scripts for _development_ and _production_ build in _package.json_ file

```sh
"scripts": {
    "start": "webpack serve --config webpack/webpack.config.js --env env=dev --open",
    "build": "webpack --config webpack/webpack.config.js --env env=prod",
    "test": "echo \"Error: no test specified\" && exit 1"
  }
```

# React Refresh

This is used to preserve and maintain the component state when we updating the component

#### Add the following dev-dependency

```sh
yarn add -D @pmmmwh/react-refresh-webpack-plugin react-refresh
```

#### Add this piece of code in your **_webpack.dev.js_**

```sh
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin")
devServer: {
    hot: true,
    open: true
  },
  plugins: [
    new webpack.DefinePlugin({"process.env.name": JSON.stringify("Kick start Typescript")}),
    new ReactRefreshWebpackPlugin()
  ]
```

# Implement ES-Lint in your project

#### Add **_eslint_** in **_dev-dependencies_** to your project

```sh
yarn add eslint eslint-plugin-eslint-comments eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-react-hooks
```

#### Create new file **_.eslintrc.js_** and add the following piece of code

```sh
module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  extends: [
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:jsx-a11y/recommended',
    'plugin:eslint-comments/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  rules: {
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['error'],
    '@typescript-eslint/no-var-requires': 'off',
    'react/prop-types': 'off',
    'react/jsx-uses-react': 'off',
    'react/react-in-jsx-scope': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
  },
}
```

#### Add a script for **_lint_** in **_package.json_** file

```sh
"scripts": {
    "start": "webpack serve --config webpack/webpack.config.js --env env=dev",
    "build": "webpack --config webpack/webpack.config.js --env env=prod",
    "test": "echo \"Error: no test specified\" && exit 1",
    "lint": "eslint --fix \"./src/**/*.{js,jsx,ts,tsx,json}\""
  }
```

# Integrating Prettier

#### Add **_prettier_** in **_dev-dependencies_** to project

```sh
yarn add -D prettier eslint-config-prettier eslint-plugin-prettier
```

#### Create a new file name as **_.prettierrc.js_** and add the following piece of code

```sh
module.exports = {
  semi: false,
  trailingComma: 'es5',
  singleQuote: true,
  printWidth: 80,
  tabWidth: 2,
  endOfLine: 'auto',
}
```

#### Add the following code to your **_.eslintrc.js_** file

```sh
  'prettier',
  'plugin:prettier/recommended',
```

#### Add **_script_** to format your code from terminal

```sh
  "scripts": {
    "start": "webpack serve --config webpack/webpack.config.js --env env=dev",
    "build": "webpack --config webpack/webpack.config.js --env env=prod",
    "test": "echo \"Error: no test specified\" && exit 1",
    "lint": "eslint --fix \"./src/**/*.{js,jsx,ts,tsx,json}\"",
    "format": "prettier --write \"./src/**/*.{js,jsx,ts,tsx,json,css,scss,md}\""
  },
```

# HUSKY & LINT-STAGED

Which is purely used to prevent commit code with eslint errors and unformatted code to git

#### Add the following **_dev-dependencies_**

```sh
yarn add -D husky@4 lint-staged
```

#### Add the following piece of code in **_package.json_** file

```sh
  "lint-staged": {
    "src/**/*.{js,jsx,ts,tsx,json}": [
      "eslint --fix"
    ],
    "src/**/*.{js,jsx,ts,tsx,json,css,scss,md}": [
      "prettier --write"
    ]
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  }
```

# Integrate Babel Runtime

This package is used to access **_async & await_** in your project

#### Add the following package to integrate **_babal runtime_**

```sh
yarn add -D @babel/runtime @babel/plugin-transform-runtime
```

#### Add Piece of coding in your .babelrc file

```sh
"plugins": [
    [
      "@babel/plugin-transform-runtime",
      {
        "regenerator": true
      }
    ]
```

# Integrate WebPack Analyzer

To analyze your build file size and stat

#### Add the package to **_dev-dependencies_**

```sh
yarn add -D copy-webpack-plugin webpack-bundle-analyzer
```

#### Add piece of coding in your **_webpack.prod.js_** file

```sh
const webpack = require('webpack')
const BundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.name': JSON.stringify('Kick start React App'),
    }),
    new BundleAnalyzer(),
  ],
}

```

# NOTE

```sh
1. SPA => Single Page Application
2. MPA => Multiple Page Application
```
