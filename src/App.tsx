import { Counter } from './Counter'
import './styles.css'
import logo from './typescript.png'
import image from './wikimedia.svg'

const App = () => {
  const name = 'Tamil vanan'
  return (
    <>
      <h1>
        Hello This is sample App is Refreshed - {process.env.NODE_ENV} -{name}
        {process.env.name}
      </h1>
      <img src={logo} alt="Logo" width="400" height="400" />
      <img src={image} alt="Wikimedia" width="400" height="400" />
      <Counter />
    </>
  )
}

export default App
